
Description
-----------

HubSpot Blog Integration connects to the HubSpot API to retrieve published blog
content created on the HubSpot.com site.


Requirements
------------

This module currently requires Drupal 7.x and a HubSpot account with access to
the HubSpot API. The HubSpot module must be configured with at least the HubSpot
Portal ID configured to use this module.

Installation
------------

Install this module as usual, then head to the HubSpot blog integration settings
under Configuration (admin/config/services/hubspot_blog_integration).

Configuration
-------------

API CONNECTION:

On the configuration page first connect the module to the HubSpot account.
Logging into the HubSpot account may be required validate the OAuth connection.
(Note: Connecting the HubSpot API in the HubSpot integration module does not
connect the API to this module.)

BLOG SELECTION SETTINGS:

For first time configuration and when newly added blog site are not visible,
update the blog site listing. Select the blog site from which to pull content.

BLOG CONTENT RETRIEVAL SETTINGS:

Select the number of most recently published blog posts to save.

Once all configurations are set, select the 'Retrieve blog content from HubSpot
connection' button to retrieve the most recent content from the HubSpot API.

NOTE - HubSpot COS Update
-------------------------

This version now supports the use of HubSpot's COS API for retrieving blog content
through the HubSpot API.

Sponsorship
-----------

This project is sponsored by SyncShow Interactive, a results-driven, digital
communication agency with a focus on inbound marketing and eCommerce conversion
strategies. SyncShow Interactive is obsessed with performance-driven solutions,
creation and implementation of digital communication tools and strategies.
SyncShow Interactive is a Certified Gold HubSpot Partner.

In need of a digital communication agency focused on results? Contact us.

http://syncshow.com/
