<?php
/**
 * @file
 * Provides HubSpot COS blog integration.
 */

/**
 * Function to retrieve available blogs from HubSpot COS Blog.
 */
function _hubspot_blog_integration_get_blogs() {
  $param_array = array();
  $hs_url = _hubspot_bog_integration_get_api_uri('list_blogs');//
  $result = hubspot_blog_integration_oauth_request($hs_url, $param_array);
  $result['data'] = json_decode($result['data']);
  return $result;
}

/**
 * Function to get blog listing data from service.
 */
function _hubspot_blog_integration_blog_list_data() {
  $return = FALSE;
  $blogs_array = array(HUBSPOT_BLOG_INTEGRATION_SELECT_NONE => t('No Blog Selected'));
  $blogs_data = _hubspot_blog_integration_get_blogs();
  if (array_key_exists('data', $blogs_data)) {
    foreach ($blogs_data['data']->objects as $blog_array) {
      $blogs_array[$blog_array->id] = (trim($blog_array->public_title) != '') ? "{$blog_array->public_title}" : "{$blog_array->root_url}";
      $return = TRUE;
    }
  }
  variable_set('hubspot_blog_integration_available_blog_list', $blogs_array);
  return $return;
}

function _hubspot_bog_integration_get_api_uri($command) {
  switch ($command) {
    case 'list_blogs':
      return HUBSPOT_BLOG_INTEGRATION_API_URL . '/content/api/v2/blogs';
      break;
    case 'list_blog_posts':
      return HUBSPOT_BLOG_INTEGRATION_API_URL . '/content/api/v2/blog-posts';
      break;
  }
  return FALSE;
}

/**
 * Function to retreive blog post data from HubSpot COS Blog.
 */
function _hubspot_blog_integration_get_blog_posts($guid = '', $max = 5, $offset = 0) {
  $return = array();
  if (!empty($guid)) {
    $param_array = array(
      'limit' => 1,

    );
    $hs_url = _hubspot_bog_integration_get_api_uri('list_blog_posts');

    /* Get total blog posts then work backwards due to HubSpot sorting oldest first. */
    $result = hubspot_blog_integration_oauth_request($hs_url, $param_array);
    $result['data'] = json_decode($result['data']);
    $return['count'] = $result['data']->total_count;
    

    $my_offset = ($return['count'] - $offset) - $max;
    $param_array = array(
      'limit' => $max,
      'offset' => ($my_offset > 0 ? $my_offset : 0),
    );


    $result = hubspot_blog_integration_oauth_request($hs_url, $param_array);
    $result['data'] = json_decode($result['data']);

    if ($return['count'] < 1 || count($result['data']) < 1){
      drupal_set_message("No posts returned",'error');
      return FALSE;
    }

    if(array_key_exists('objects',$result['data'])){
      $result['data']->robjects = array_reverse($result['data']->objects);
      foreach ($result['data']->robjects as $blog_post_key => $blog_post_obj) {
        $guid = $blog_post_obj->id;
        $return['data'][$guid]['guid'] = $guid;
        $return['data'][$guid]['draft'] = ($blog_post_obj->is_draft) ? FALSE : TRUE;
        $return['data'][$guid]['title'] = $blog_post_obj->name;
        $return['data'][$guid]['body'] = $blog_post_obj->post_body;
        $return['data'][$guid]['url'] = $blog_post_obj->url;
        $return['data'][$guid]['publish_date'] = intval($blog_post_obj->publish_date / 1000);
      }
    }else {
      drupal_set_message($result['data']->message);
    }

    return $return;
  }
  return FALSE;
}

