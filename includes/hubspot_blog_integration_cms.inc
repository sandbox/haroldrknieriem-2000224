<?php
/**
 * @file
 * Provides HubSpot CMS blog integration.
 */

/**
 * Function to retrieve available blogs from HubSpot CMS Blog.
 */
function _hubspot_blog_integration_get_blogs() {
  $param_array = array();
  $hs_url = HUBSPOT_BLOG_INTEGRATION_BLOG_URL . "list.json";
  $result = hubspot_blog_integration_oauth_request($hs_url, $param_array);
  $result['data'] = json_decode($result['data']);
  return $result;
}

/**
 * Function to get blog listing data from service.
 */
function _hubspot_blog_integration_blog_list_data() {
  $return = FALSE;
  $blogs_array = array(HUBSPOT_BLOG_INTEGRATION_SELECT_NONE => t('No Blog Selected'));
  $blogs_data = _hubspot_blog_integration_get_blogs();
  if (array_key_exists('data', $blogs_data)) {
    foreach ($blogs_data['data'] as $blog_array) {
      $blogs_array[$blog_array->guid] = (trim($blog_array->blogTitle) != '') ? "{$blog_array->blogTitle}" : "{$blog_array->webUrl}";
      $return = TRUE;
    }
  }
  variable_set('hubspot_blog_integration_available_blog_list', $blogs_array);
  return $return;
}

/**
 * Function returns the API URL associated to requested command.
 */
function _hubspot_bog_integration_get_api_uri($command) {
    switch ($command) {
      case 'list_blogs':
        return HUBSPOT_BLOG_INTEGRATION_BLOG_URL . "list.json";
        break;
      case 'list-blog-posts':
        return HUBSPOT_BLOG_INTEGRATION_BLOG_URL . '/content/api/v2/blog-posts';
        break;
    }
  return FALSE;
}

/**
 * Function to retrieve blog post data from HubSpot CMS Blog.
 */
function _hubspot_blog_integration_get_blog_posts($guid = '', $max = 5, $offset = 0) {
  $return = array();
  if (!empty($guid)) {
    $param_array = array(
      'max' => $max,
      'offset' => $offset,
    );
    $hs_url = HUBSPOT_BLOG_INTEGRATION_BLOG_URL . "{$guid}/posts.json";
    $result = hubspot_blog_integration_oauth_request($hs_url, $param_array);
    $result['data'] = json_decode($result['data']);
    $return['count'] = count($result['data']);

    if ($return['count'] < 1) {
      drupal_set_message("No posts returned",'error');
      return FALSE;
    }
    foreach ($result['data'] as $blog_post_obj) {
      $guid = $blog_post_obj->guid;
      $return['data'][$guid]['guid'] = $guid;
      $return['data'][$guid]['draft'] = ($blog_post_obj->draft) ? FALSE : TRUE;
      $return['data'][$guid]['title'] = $blog_post_obj->title;
      $return['data'][$guid]['body'] = $blog_post_obj->body;
      $return['data'][$guid]['url'] = $blog_post_obj->url;
      $return['data'][$guid]['publish_date'] = intval($blog_post_obj->publishTimestamp / 1000);
    }
    return $return;
  }
  return FALSE;
}

