<?php
/**
 * Created by PhpStorm.
 * User: haroldrknieriem
 * Date: 2/27/14
 * Time: 1:29 PM
 */

define('HUBSPOT_BLOG_INTEGRATION_OAUTH_URL', 'https://app.hubspot.com/auth/authenticate');
define('HUBSPOT_BLOG_INTEGRATION_OAUTH_REFRESH', 'https://api.hubapi.com/auth/v1/refresh');

/**
 * Implements hook_help().
 */
function hubspot_blog_integration_oauth_help($path, $arg) {
  $output = '';
  if ($path == "admin/help#hubspot_blog_integration") {
    $output = '<h3>' . t('About') . '</h3>';
    $output .= '<p>';
    $output .= t('The HubSpot OAuth module provides OAuth connection with the HubSpot API, allowing other HubSpot modules to connect to the HubSpot API.');
    $output .= '</p>';
  }
  return $output;
}

/**
 * Implements hook_menu().
 */
function hubspot_blog_integration_oauth_menu() {
  $items = array();

  $items['hubspot-api-oauth/oauth'] = array(
    'title' => 'HubSpot OAuth redirect',
    'description' => 'Collects HubSpot OAuth tokens.',
    'page callback' => 'hubspot_blog_integration_oauth_connect',
    'access arguments' => array('administer hubspot oauth'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function hubspot_blog_integration_oauth_permission() {
  return array(
    'access hubspot oauth' => array(
      'title' => t('Access data via HubSpot OAuth connection'),
      'description' => t('Allow connection to HubSpot API data via OAuth.'),
    ),
  );
}

/**
 * Function for submitting oauth request to HubSpot.
 */
function hubspot_blog_integration_oauth_submit($form, &$form_state) {
  $data = array(
    'client_id' => $form_state['values']['api_module_client_id'],
    'portalId' => variable_get('hubspot_blog_integration_portalid'),
    'redirect_uri' => url('hubspot-blog-integration/oauth', array(
      'query' => drupal_get_destination(),
      'absolute' => TRUE,
    )),
    'scope' => $form_state['values']['api_module_client_scope'],
  );

  $form_state['redirect'][] = url(HUBSPOT_BLOG_INTEGRATION_OAUTH_URL, array('query' => $data));
}

/**
 * Function for oauth connection - Uses hubspot_blog_integration.
 */
function hubspot_blog_integration_oauth_connect() {

  $myGET = array();
  foreach($_GET as $key => $value) {
    $myGet[] = "{$key}={$value}";
  }

  if (!empty($_GET['access_token']) && !empty($_GET['refresh_token']) && !empty($_GET['expires_in'])) {
    variable_set('hubspot_blog_integration_access_token', $_GET['access_token']);
    variable_set('hubspot_blog_integration_refresh_token', $_GET['refresh_token']);
    variable_set('hubspot_blog_integration_expires_in', $_GET['expires_in']);
    variable_set('hubspot_blog_integration_scope_returned', drupal_json_encode($_GET));

    $hs_now = REQUEST_TIME;
    $hs_expire_in = intval($_GET['expires_in']);
    variable_set('hubspot_blog_integration_expires_at', $hs_now + $hs_expire_in);
    drupal_set_message(t('Successfully connected to the HubSpot API.'));

    hubspot_blog_integration_blog_list_data();
  }
  else {
    drupal_set_message(t('We were unable to complete the HubSpot Authorization. Please click the button again and choose the AUTHORIZE option.'), 'error', FALSE);
  }

  if (!empty($_GET['error']) && $_GET['error'] == "access_denied") {
    drupal_set_message(t('You were denied the request for authentication with HubSpot. Please click the button again and choose the AUTHORIZE option.'), 'error', FALSE);
  }
  drupal_goto();
}

/**
 * Function for oauth disconnection - Uses hubspot_blog_integration.
 */
function hubspot_blog_integration_oauth_disconnect($form, &$form_state) {
  variable_del('hubspot_blog_integration_access_token');
  variable_del('hubspot_blog_integration_refresh_token');
  variable_del('hubspot_blog_integration_expires_in');
  variable_del('hubspot_blog_integration_expires_at');
  variable_del('hubspot_blog_integration_available_blog_list');
  variable_del('hubspot_blog_integration_blog_list');
  drupal_set_message(t('Successfully disconnected from HubSpot.'), 'status', FALSE);
}

/**
 * Refreshes HubSpot OAuth Access Token when expired.
 */
function hubspot_blog_integration_oauth_refresh() {
  $now = time();

  $data = array(
    'refresh_token' => variable_get('hubspot_blog_integration_refresh_token'),
    'client_id' => HUBSPOT_BLOG_INTEGRATION_CLIENT_ID,
    'grant_type' => 'refresh_token',
  );

  $data = drupal_http_build_query($data);

  $options = array(
    'headers' => array(
      'Content-Type' => 'application/x-www-form-urlencoded; charset=utf-8',
    ),
    'method' => 'POST',
    'data' => $data,
  );

  $return = drupal_http_request(HUBSPOT_BLOG_INTEGRATION_OAUTH_REFRESH, $options);

  if ($return->code == '200') {
    $return_data = json_decode($return->data, TRUE);

    $hubspot_access_token = $return_data['access_token'];
    variable_set('hubspot_blog_integration_access_token', $hubspot_access_token);

    $hubspot_refresh_token = $return_data['refresh_token'];
    variable_set('hubspot_blog_integration_refresh_token', $hubspot_refresh_token);

    $hubspot_expires_in = $return_data['expires_in'];
    variable_set('hubspot_blog_integration_expires_in', $hubspot_expires_in);
    variable_set('hubspot_blog_integration_expires_at', $now + $hubspot_expires_in);

    return TRUE;
  }
  else {
    drupal_set_message(t('Refresh token failed with Error Code "%code: %status_message". Reconnect to your HubSpot account.',
      array(
        '%code' => $return->code,
        '%status_message' => $return->status_message,
      )
    ), 'error', FALSE);
    watchdog('hubspot', 'Refresh token failed with Error Code "%code: %status_message". Visit the HubSpot module settings page and reconnect to your HubSpot account.',
      array(
        '%code' => $return->code,
        '%status_message' => $return->status_message,
      ), WATCHDOG_INFO);

    return FALSE;
  }
}

/**
 * Function to run data requests from HubSpot API.
 */
function hubspot_blog_integration_oauth_request($url, array $param_array) {

  $return = array();

  $access_token = variable_get('hubspot_blog_integration_access_token');
  if (empty($access_token)) {
    $return['Error'] = t('This site is not connected to a HubSpot Account.');
    return $return;
  }

  unset ($param_array['access_token']);
  array_unshift($param_array, array('access_token' => $access_token));
  $hs_url = url($url . '?' . drupal_http_build_query($param_array));
  $result = drupal_http_request($hs_url);



  if ($result->code == HUBSPOT_BLOG_INTEGRATION_REFRESH_CODE) {
    $refresh = hubspot_blog_integration_oauth_refresh('hubspot_blog_integration');
    if ($refresh) {
      $access_token = variable_get('hubspot_blog_integration_access_token');

      unset ($param_array['access_token']);
      array_unshift($param_array, array('access_token' => $access_token));
      $hs_url = url($url . '?' . drupal_http_build_query($param_array));
      $result = drupal_http_request($hs_url);
    }
    else {
      $return['Error'] = t('This site is not refresh the API connection to a HubSpot Account.');
      return $return;
    }
  }
  if (array_key_exists('Error',$return)) {
    drupal_set_message($return['Error'],'error');
  }
  if ($result->data) {
    $return['data'] = $result->data;
  }
  return $return;
}
