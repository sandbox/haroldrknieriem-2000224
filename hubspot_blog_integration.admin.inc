<?php
/**
 * @file
 * Provides HubSpot Blog integration administration for Drupal.
 */

/**
 * Function for administration settings form.
 */
function hubspot_blog_integration_admin_settings_form($form, $form_state) {

  if (user_access('administer hubspot blog')) {

    list($module_connection, $module_connection_label) = hubspot_blog_integration_get_connection_status();

    $blog_list_array = variable_get('hubspot_blog_integration_available_blog_list', array(HUBSPOT_BLOG_INTEGRATION_SELECT_NONE => t('No Blog Selected')));
    $blog_list_default = variable_get('hubspot_blog_integration_blog_list', HUBSPOT_BLOG_INTEGRATION_SELECT_NONE);

    $form = array(
      'api_portal_settings' => array(
        '#type' => 'fieldset',
        '#title' => t('Portal Settings'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
        'hubspot_blog_integration_portalid' => array(
          '#title' => t('HubSpot Portal ID'),
          '#type' => 'textfield',
          '#required' => FALSE,
          '#default_value' => variable_get('hubspot_blog_integration_portalid', ''),
          '#description' => t('Enter the Hubspot Portal ID for this site.  It can be found by <a href="https://login.hubspot.com/login" target="_blank">logging into HubSpot</a> going to the Dashboard and examining the url. Example: "https://app.hubspot.com/dashboard-plus/12345/dash/".  The number after "dashboard-plus" is your Portal ID.'),
        ),
        'hubspot_blog_integration_portalid_show' => array(
          '#type' => 'hidden',
          '#value' => 'no_show',
        ),
        'header' => array(
          '#type' => 'item',
          '#title' => $module_connection_label,
          '#markup' => t('Click on the button below to start connection with the HubSpot API.'),
        ),
        'hubspot_blog_integration_authentication' => array(
          '#type' => 'submit',
          '#value' => t('Disconnect HubSpot Account'),
          '#validate' => array(),
          '#submit' => array('hubspot_blog_integration_oauth_disconnect'),
        ),
      ),
      'api_blog_list' => array(
        '#type' => 'fieldset',
        '#title' => t('Blog selection settings'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
        'hubspot_blog_integration_blog_verion' => array(
          '#type' => 'select',
          '#title' => t('HubSpot Blog Version (CMS or COS)'),
          '#options' => hubspot_blog_integration_get_select('blog_version'),
          '#default_value' => variable_get('hubspot_blog_integration_blog_verion', 2),
        ),
        'hubspot_blog_integration_blog_list' => array(
          '#type' => 'radios',
          '#title' => 'Select blogs to be included in the downloads.',
          '#options' => $blog_list_array,
          '#default_value' => $blog_list_default,
        ),
        'hubspot_blog_integration_blog_list_update' => array(
          '#type' => 'submit',
          '#value' => t('Update HubSpot available blogs.'),
          '#validate' => array(),
          '#submit' => array('hubspot_blog_integration_blog_list_data'),
        ),
      ),
      'api_blog_advanced' => array(
        '#type' => 'fieldset',
        '#title' => t('Blog content advanced options'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        'hubspot_blog_integration_blog_count' => array(
          '#type' => 'select',
          '#title' => t('Count of most recent published content to retrieve.'),
          '#options' => hubspot_blog_integration_get_select('blog_count'),
          '#default_value' => variable_get('hubspot_blog_integration_blog_count', 3),
        ),
        'hubspot_blog_integration_blog_owner' => array(
          '#type' => 'select',
          '#title' => t('Set new content UID to this user'),
          '#options' => hubspot_blog_integration_get_user_list(),
          '#default_value' => variable_get('hubspot_blog_integration_blog_owner', 3),
        ),
        'hubspot_blog_integration_allow_search' => array(
          '#type' => 'select',
          '#title' => t('Allow content to be view in site search.'),
          '#options' => hubspot_blog_integration_get_select('allow_search'),
          '#default_value' => variable_get('hubspot_blog_integration_allow_search', 1),
        ),
        'hubspot_blog_integration_cron_timing' => array(
          '#type' => 'select',
          '#title' => t('Cron timing for content retrieval.'),
          '#options' => hubspot_blog_integration_get_select('cron_timing'),
          '#default_value' => variable_get('hubspot_blog_integration_cron_timing', 3600),
        ),
        'hubspot_blog_integration_cron_timing_reset' => array(
          '#type' => 'submit',
          '#value' => t('Reset cron timer.'),
          '#validate' => array(),
          '#submit' => array('hubspot_blog_integration_blog_reset_cron_timer'),
        ),
      ),
      'api_module_prefix' => array(
        '#type' => 'hidden',
        '#value' => 'hubspot_blog_integration',
      ),
      'api_module_client_id' => array(
        '#type' => 'hidden',
        '#value' => HUBSPOT_BLOG_INTEGRATION_CLIENT_ID,
      ),
      'api_module_client_scope' => array(
        '#type' => 'hidden',
        '#value' => HUBSPOT_BLOG_INTEGRATION_SCOPE,
      ),


    );

    if (variable_get('hubspot_blog_integration_portalid', '')) {
      if (variable_get('hubspot_blog_integration_portalid_show', 'show') == 'no_show') {
        unset ($form['api_portal_settings']['hubspot_blog_integration_portalid']);
        $form['api_portal_settings']['hubspot_blog_integration_portalid_show'] = array(
          '#type' => 'submit',
          '#value' => t('Change HubSpot Portal ID'),
          '#validate' => array(),
          '#submit' => array('hubspot_blog_integration_blog_reset_portal_id'),
        );
        $form['api_portal_settings']['header']['#markup'] = t('Your HubSpot Portal ID is currently %portal_id.<br />
          Click on the button below to start connection with the HubSpot API.<br />',
          array('%portal_id' => variable_get('hubspot_blog_integration_portalid'))
        );
      }

      if ($module_connection) {
        unset ($form['api_portal_settings']['hubspot_blog_integration_portalid_show']);
        $form['api_portal_settings']['header']['#markup'] = t('Your HubSpot account is connected to Portal ID %portal_id.<br />',
          array('%portal_id' => variable_get('hubspot_blog_integration_portalid'))
        );
        $form['api_portal_settings']['#collapsed'] = TRUE;

        if (count($blog_list_array) > 1 && $blog_list_default != HUBSPOT_BLOG_INTEGRATION_SELECT_NONE) {
          $form['api_blog_list']['#collapsed'] = TRUE;
          $form['api_blog_advanced']['#collapsed'] = FALSE;
        }
      }
      else {
        // If HubSpot API not connected then remove blog_list and clog_content.
        // field-sets.
        unset($form['api_blog_list']);
        $form['api_portal_settings']['hubspot_blog_integration_authentication']['#value'] = t('Connect HubSpot Account');
        $form['api_portal_settings']['hubspot_blog_integration_authentication']['#submit'] = array('hubspot_blog_integration_oauth_submit');
      }

    }
    else {
      // If HubSpot portal id not setup then remove blog_list, clog_content and
      // connect_settings.
      unset($form['api_blog_list']);
      unset($form['api_portal_settings']['header']);
      unset($form['api_portal_settings']['hubspot_blog_integration_authentication']);
    }

    return system_settings_form($form);
  }
}

/**
 * Function for content retrieval form.
 */
function hubspot_blog_integration_admin_content_form() {

  list($module_connection, $module_connection_label) = hubspot_blog_integration_get_connection_status();
  $blog_list = variable_get('hubspot_blog_integration_blog_list', HUBSPOT_BLOG_INTEGRATION_SELECT_NONE);

  if (user_access('retrieve hubspot blog')) {
    $blog_count = variable_get('hubspot_blog_integration_blog_count', 3);
    $form = array(
      'api_blog_content' => array(
        '#type' => 'fieldset',
        '#title' => t('Blog content retrieval settings'),
        '#collapsible' => FALSE,
        '#collapsed' => TRUE,
        'header' => array(
          '#type' => 'item',
          '#title' => '',
          '#markup' => t('Retrieve @count most recent blog postings from HubSpot connection.', array('@count' => $blog_count)),
        ),
        'hubspot_blog_integration_blog_update' => array(
          '#type' => 'submit',
          '#value' => t('Retrieve content'),
          '#validate' => array(),
          '#submit' => array('hubspot_blog_integration_node_data'),
        ),
      ),
    );
    if (!$module_connection || $blog_list == HUBSPOT_BLOG_INTEGRATION_SELECT_NONE) {
      $form = array(
        'header' => array(
          '#type' => 'item',
          '#title' => $module_connection_label,
          '#markup' => t('This site is not connected with the HubSpot API. Please contact your system administrator.'),
        ),
      );
    }
  }
  return $form;
}

/**
 * Returns an array for predefined selector.
 */
function hubspot_blog_integration_get_select($type = 'blog_count') {
  switch ($type) {
    case 'allow_search':
      $return = array(
        '0' => t('Hide Content from Search.'),
        '1' => t('Allow content to be searched.'),
      );
      break;

    case 'cron_timing':
      $return = array(
        '3600' => t('Hourly'),
        '43200' => t('Every twelve hours'),
        '86400' => t('Once a day'),
      );
      break;

    case 'blog_version':
      $return = array(
        '1' => t('CMS Blog.'),
        '2' => t('COS Blog.'),
      );
      break;

    default:
      $return = array('3' => '3', '5' => '5', '10' => '10');
  }

  return $return;
}

/**
 * Returns an array list of 'authenticated' users in for of 'uid'=>'name'.
 */
function hubspot_blog_integration_get_user_list() {
  $users = entity_load('user');
  $user_array = array();
  foreach ($users as $user) {
    if (array_key_exists(2, $user->roles)) {
      $user_array[$user->uid] = $user->name;

    }
  }
  return $user_array;
}

/**
 * Returns an array list of connection status (status,label).
 */
function hubspot_blog_integration_get_connection_status() {
  $now = time();
  $module_connection = TRUE;
  $module_connection_label = t('Connection Available');

  $expires_at = variable_get('hubspot_blog_integration_expires_at', 0);
  if ($expires_at < $now || !variable_get('hubspot_blog_integration_refresh_token', '')) {
    $module_connection = FALSE;
    $module_connection_label = t('Not Connected');
  }
  return array($module_connection, $module_connection_label);
}
